<?php

require_once '../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(realpath(__DIR__ . '/..'));
$dotenv->load();

$token = $_ENV['TELEGRAMM_API_TOKEN'];

$url = 'https://api.telegram.org/bot' . $token . '/getUpdates';

$sendMessageUrl = 'https://api.telegram.org/bot' . $token . '/sendMessage';

$exchangeRate = json_decode(file_get_contents('https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5'),
    true);

$currencies = array_column($exchangeRate, 'ccy');

$lastUpdateId = null;
$_url = $url;

$data = [];

foreach ($exchangeRate as $item) {
    $data[] = [
        $item['ccy'] => [$item['sale'], $item['base_ccy']],
    ];
}

$data = array_merge(...$data);

function getResp(int $chatId, string $text, int $messageId): array
{
    return [
        'chat_id' => $chatId,
        'text' => $text,
        'reply_to_message_id' => $messageId,
    ];
}

do {
    if (null !== $lastUpdateId) {
        $_url = $url . '?offset=' . $lastUpdateId;
    }

    $response = json_decode(file_get_contents($_url), true);

    foreach ($response['result'] as $result) {
        if ($lastUpdateId == $result['update_id']) {
            continue;
        }

        $lastUpdateId = $result['update_id'];

        $messageId = $result['message']['message_id'];
        $chatId = $result['message']['chat']['id'];
        $text = $result['message']['text'] ?? null;

        $greetings = 'Привет! Это Currency_Converter_Bot. Я помогу вам конвертировать в гривну такие валюты: ' . implode(', ', $currencies) . '. Вам нужно ввести сумму и валюту в формате "1 USD" или "1.1 USD". Курс валют я беру из официального источника: Приват банк.';

        if ($text == '/start') {
            $resp = getResp($chatId, $greetings, $messageId);

            file_get_contents($sendMessageUrl . '?' . http_build_query($resp));
            continue;
        }

        $request = explode(' ', mb_strtoupper($text));

        if (!isset($request[1])) {

            $resp = getResp($chatId, 'Пожалуйста выберите валюту. Доступные валюты: ' . implode(', ', $currencies), $messageId);

            file_get_contents($sendMessageUrl . '?' . http_build_query($resp));
            continue;
        }

        if (!is_numeric($request[0])) {
            $resp = getResp($chatId, 'Укажите пожалуйста сумму и валюту в формате: 1 USD или 1.1 USD', $messageId);

            file_get_contents($sendMessageUrl . '?' . http_build_query($resp));
            continue;
        }

        if (!in_array($request[1], $currencies)) {

            $resp = getResp($chatId, 'Указанной валюты нет в списке доступных валют. Доступные валюты: ' . implode(', ', $currencies), $messageId);

            file_get_contents($sendMessageUrl . '?' . http_build_query($resp));
            continue;
        }

        $resp = getResp($chatId, $request[0] * $data[$request[1]][0] . ' ' . $data[$request[1]][1], $messageId);

        file_get_contents($sendMessageUrl . '?' . http_build_query($resp));
    }

    sleep(2);
} while (true);
